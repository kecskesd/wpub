function update_input() {
    if(document.getElementById('search-input').value !== document.getElementById('search-compare').value)
        document.getElementById('search-button').style.backgroundImage = 'url(/img/search.png)'
}

function sort_new() {
    let main = document.getElementById('articles');
    document.getElementById('up').style.display = 'block';
    document.getElementById('down').style.display = 'none';
    [].map.call(main.children, Object).sort(function(a, b) {
        return  +b.id.match(/\d+/) - +a.id.match(/\d+/);
    }).forEach(function(elem) {
        main.appendChild(elem);
    });
}

function sort_old() {
    let main = document.getElementById('articles');
    document.getElementById('up').style.display = 'none';
    document.getElementById('down').style.display = 'block';
    [].map.call(main.children, Object).sort(function(a, b) {
        return  +a.id.match(/\d+/) - +b.id.match(/\d+/);
    }).forEach(function(elem) {
        main.appendChild(elem);
    });
}

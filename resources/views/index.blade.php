<!doctype html>
<html lang="sk">

<head>
    <meta charset="utf-8">
    <title>RSS Feed Reader</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="icon" href="/favicon.ico">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap">

    <script src="/js/jquery.js"></script>
    <script src="/js/scripts.js"></script>
</head>

<body onload="sort_new()">
<header>
    <form>
        <input oninput="update_input()" id="search-input" type="text" name="search" placeholder="Enter RSS Feed" value="{{$search}}">
        <input id="search-compare" type="hidden" value="{{$search}}">
        <button id="search-button" type="submit">GO</button>
    </form>
</header>

<main>
    <h1>
        @if($rss->get_title())
            {{$rss->get_title()}}
        @else
            Start by entering a RSS feed
        @endif
    </h1>
    <div id="sort">
        <p>Sort by date</p>
        <button onclick="sort_new()" id="down" style="display: none">&uarr;</button>
        <button onclick="sort_old()" id="up">&darr;</button>
    </div>
    <div id="articles">
        @foreach($articles as $article)
            <div class="article" id="{{$article->get_date('YmdHi')}}">
                @foreach($article->get_enclosures() as $image)
                    @if($image->get_link() != null)
                        <a target="_blank" href="{{url($article->get_permalink())}}"><img src="{{url($image->get_link())}}" alt="{{$article->get_title()}}"></a>
                        @break
                    @endif
                @endforeach
                <div class="info">
                    <a class="title" target="_blank" href="{{url($article->get_permalink())}}">{{$article->get_title()}}</a>
                    <p class="description" id="D-{{$loop->index}}">{{$article->get_description()}}</p>
                    <script>
                        $container = $('#D-{{$loop->index}}');
                        $container.html($container.text().substring(0,350));
                    </script>
                    @if($article->get_date() != null)
                        <p class="published">Published on {{$article->get_date('d.m.Y')}} at {{$article->get_date('H:i')}}</p>
                    @endif
                </div>

            </div>
        @endforeach
    </div>
</main>

<script>
    if($.trim($('#articles').html())==''){
        document.getElementById('search-button').style.backgroundImage = 'url(/img/search.png)';
        document.getElementById('sort').style.display = 'none';
    }
</script>

</body>

</html>


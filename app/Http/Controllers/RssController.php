<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Vedmant\FeedReader\Facades\FeedReader;

class RssController extends Controller
{
    public function rss(Request $request){
        $search = $request->input('search');
        $rss = FeedReader::read($search);
        $articles = $rss->get_items();
        return view('index', ['rss' => $rss, 'articles' => $articles, 'search' => $search]);
    }
}
